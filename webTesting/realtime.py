def main():
    import time
    import cv2
    import numpy as np
    import mediapipe as mp
    import tensorflow as tf

    # Function to extract keypoints
    def extract_keypoints(results):
        pose = np.array([[res.x, res.y, res.z, res.visibility] for res in results.pose_landmarks.landmark]).flatten() if results.pose_landmarks else np.zeros(33 * 4)
        face = np.array([[res.x, res.y, res.z] for res in results.face_landmarks.landmark]).flatten() if results.face_landmarks else np.zeros(468 * 3)
        
        # Extract left hand landmarks
        lh = np.array([[res.x, res.y, res.z] for res in results.left_hand_landmarks.landmark]).flatten() if results.left_hand_landmarks else np.zeros(21 * 3)
        
        # Extract right hand landmarks
        rh = np.array([[res.x, res.y, res.z] for res in results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(21 * 3)
        
        return np.concatenate([pose, face, lh, rh])

    # Load TFLite model
    model = tf.keras.models.load_model('ASL_fam.h5')

    # Load actions from a text file
    with open('actions_FAM.txt', 'r') as file:
        actions = [line.strip() for line in file.readlines()]
        
    # Initialize detection variables
    sequence = []
    sentence = []
    min_confidence = 0.7  # Minimum confidence (80%)
    max_confidence = 0.9  # Maximum confidence (95%)
    freeze_duration = 2  # 2 seconds of freezing
    last_highest_confidence_time = None
    highest_confidence_actions = []  # Store the top 3 actions
    last_sentence_display_time = None

    # Initialize MediaPipe Holistic
    mp_holistic = mp.solutions.holistic
    holistic = mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5)

    cap = cv2.VideoCapture(0)

    while cap.isOpened():
        ret, frame = cap.read()
        
        image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        results = holistic.process(image)

        if results.pose_landmarks and (results.left_hand_landmarks or results.right_hand_landmarks):
            hands_detected = True
        else:
            hands_detected = False

        keypoints = extract_keypoints(results)
        sequence.append(keypoints)
        sequence = sequence[-30:]
        frame = cv2.flip(frame,1)
        if hands_detected and len(sequence) == 30:
            input_data = np.expand_dims(sequence, axis=0).astype(np.float32)
            
            res = model.predict(input_data)[0]

            # Get the top 3 actions and their confidence scores
            top_actions_indices = np.argsort(res)[-3:][::-1]
            top_actions = [actions[i] for i in top_actions_indices]
            top_confidences = [res[i] for i in top_actions_indices]

            print("Top Actions:")
            for action, confidence in zip(top_actions, top_confidences):
                print(f"{action}: {confidence:.2%}")

            part_length = len(sequence) // 3
            sequence1 = sequence[:part_length]
            sequence2 = sequence[part_length:2 * part_length]
            sequence3 = sequence[2 * part_length:]

            if freeze_duration is not None and last_highest_confidence_time is not None:
                time_elapsed = time.time() - last_highest_confidence_time
                if time_elapsed >= freeze_duration:
                    highest_confidence_actions = []

            sequence1_complete = all(keypoints.any() for keypoints in sequence1)
            sequence2_complete = all(keypoints.any() for keypoints in sequence2)
        
            if highest_confidence_actions:
                # Display the top 3 actions at the bottom right
                for i, (action, confidence) in enumerate(zip(top_actions, top_confidences)):
                    display_text = f"{i + 1}: {action} ({confidence * 100:.2f}%)"
                    text_size, _ = cv2.getTextSize(display_text, cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
                    text_x = frame.shape[1] - text_size[0] - 10
                    text_y = frame.shape[0] - 30 * (3 - i)
                    cv2.putText(frame, display_text, (text_x, text_y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 25, 255), 2, cv2.LINE_AA)

            if sequence1_complete and sequence2_complete and min_confidence <= max_confidence <= 1:
                sequence_complete = True
                sentence = [top_actions[0]]  # Store the top action in the sentence
                display_text = f'{top_actions[0]} ({top_confidences[0] * 100:.2f}%)'
                cv2.putText(frame, display_text, (3, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 25, 255), 2, cv2.LINE_AA)
                last_highest_confidence_time = time.time()
                highest_confidence_actions = top_actions
                last_sentence_display_time = time.time()

        # Display the last detected sentence for 5 seconds
        if last_sentence_display_time is not None:
            current_time = time.time()
            if current_time - last_sentence_display_time < 3:
                cv2.putText(frame, f'Sign: {" ".join(sentence)}', (3, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
            else:
                last_sentence_display_time = None
                sentence = []  # Clear the sentence
        
        cv2.imshow('ASL PREDICTION', frame)

        if cv2.waitKey(10) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
